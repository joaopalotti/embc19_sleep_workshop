# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.7
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ![Title](images/titleworkshop.jpg)

# # An Introduction to Physical Activity and Sleep Analysis from Wearable Device Data

# ## Python
# For those of you unfamilar with Python, don't be afraid to ask if you have any questions and we'll be as helpful as possible in assisting you with any questions you may have.
# ## Jupyter Notebook
# This environment you are working on today is called Jupyter Notebook. Jupyter is now probably the most well-established environment for Python Development, particularly for rapid prototyping. It is particularly useful for collaborations, visualizations and exercises like this one!
# Github supports jupyter notebook, making it even more interesting to use.
#
# This, for example is a Markdown cell, where we can introduce text or images.

# # Loading Libraries and Packages for Exercise
#
# For this exercise, we are going to load several Python Libraries and Packages, in a similar fashion to how we would do it in R:

# +
# BASIC LIBRARY IMPORT - MAKE SURE YOU ARE IN A CODE CELL
import numpy as np # for mathematical operations
import os
import pandas as pd # for dataframes
import warnings
warnings.filterwarnings('ignore')

# Matplotlib: standard plotting library for python
import matplotlib # for plotting
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.dates import DateFormatter
from matplotlib import rc
from matplotlib.dates import date2num
from IPython import display

# Please obtain this package from https://github.com/Thomite/pampro
#from pampro import data_loading, hdf5, Time_Series, Channel, channel_inference, triaxial_calibration, time_utilities, pampro_fourier, batch_processing
#from pampro import data_loading, Time_Series, Channel, channel_inference, triaxial_calibration, Bout

# sklearn is a popular ML library with implementation of most of the state-of-the-art ML algorithms
from sklearn import linear_model
from sklearn import ensemble
from sklearn import metrics
from sklearn import utils
from sklearn import model_selection
from sklearn.tree import export_graphviz

# for accelerometer data processing (done for you but good to know...)
from datetime import datetime, date, time, timedelta


# +
# OPTIONAL: FIGURE "BEAUTIFIER"

# Fiddle with figure settings here:
plt.rcParams['figure.figsize'] = (10,8)
plt.rcParams['font.size'] = 14
plt.rcParams['image.cmap'] = 'plasma'
plt.rcParams['axes.linewidth'] = 2

# -

# Today, we will be looking at identifying sleep wake transtions from Accelerometry Data from Axivity Devices. You were introduced to Sleep-Wake cycle classification during our lectures and now we will take a look at some fundamental considerations we have when developing algorithms to evaluate sleep-wake transitions in our work.
#
# This what a whole day of Activity data may look like:

# ![SleepAccelerometry](images/Data+Output+Whole-Day+Accelerometry+Adult.jpg)

# However, this data is highly processed (counts). Although some devices (mostly commercial ones) still report data at this count based-level, research grade devices like Axivity will allow researchers to start with high resolution (60-100Hz) triaxial data. Let's take a look!

# # Data Preprocessing with PAMPRO

# Given our time limit and the scope of this workshop, we will work with Preprocessed data at a 30 second resolution. However, we will briefly introduce the tool that allow us to get us there (regardless of the accelerometer).
# That tool is PAMPRO: ** Physical Activity Monitor Processing ** which can be found here: https://github.com/Thomite/pampro

# For examples of PAMPRO for data preprocessing, please take a look at : https://github.com/Thomite/pampro/tree/master/examples

# ![ActivityProcess](images/activityprocess.PNG)

# On this image we introduce what a standard pipeline for processing Actigraphy/Accelerometer data might look like using PAMPRO. For any questions regarding PAMPRO preprocessing please find Ignacio/Bing or email: Ella.Hutchinson@mrc-epid.cam.ac.uk (for support and newest version release).

# Standard data loading with Pandas:
dataset1 = pd.read_csv('./datasets/acc_30s.csv') 

# Extract Hour, Minute, Second Columns
dataset1["ts"] = [datetime.strptime(ts,"%d/%m/%Y %H:%M:%S") for ts in dataset1["timestamp"]] 
dataset1['date'] = dataset1['ts'].dt.date
dataset1['hour'] = dataset1['ts'].dt.hour

# +
# Let's Plot the Data (both Acc and HR)
plt.rcParams['axes.facecolor'] = 'white'
fig, ax = plt.subplots(1, 1, figsize= (14, 8))

ax.set_title("Activity and Heart Rate for participant")
ax.tick_params(axis='x', which='both',bottom=False,top=False, labelbottom=True, rotation=45)
ax.xaxis.set_major_formatter(DateFormatter('%m-%d'))

ax.grid(color='#b2b2b7', linestyle='--',linewidth=1, alpha=0.5)
ax.plot(dataset1['ts'],dataset1['ENMO_mean'], label='ENMO',linewidth=1.5, color ='#063852')
ax.set_ylabel('ENMO')

# Weekend Colour 
start = 0
end = 0

# 0 represents Monday and 6 Sunday
dataset1["isWeekday"] = dataset1['date'].apply(lambda x : True if x.weekday() in [5,6] else False)
pos = date2num(dataset1[dataset1["isWeekday"] == True]["ts"])

# Print blocks of gray color every
for i in range(0, len(pos)-1):
    if abs(pos[i] - pos[i+1]) > 0.1:
        end = i
        ax.axvspan(pos[start], pos[end], color='#DDDDDD')
        start = i+1

# In case the night period is in the data and we still have a last block to print
if len(pos) > 0:
    # Print the last block
    ax.axvspan(pos[start], pos[-1], color='#DDDDDD')

# Adjust Params
fig.autofmt_xdate()
plt.tight_layout()


# -

# # Sleep Wake Classification

# Polysomnography (PSG) is the gold standard approach for diagnosing specific sleep disorders.
# ![PSG](images/psg.png)
# But its usage is impractical for identification of more prevalent issues with sleep loss and sleep quality. 
#
# An attractive alternative to PSG is the use of wearables, such as accelerometer-based technology (Actigraphy), which may be used as a diagnostic aid for specific sleep disorders such as circadian rhythm disorders. 
#
# But these wearables only provide the amount of activity in a window of time, we need some <b> algorithm</b> to infer what is going on with the person that is wearing the device.

# ## Traditional Algorithms
#
# A large set of algorithms have been developed in the last decades to estimate 'wake' and 'sleep' states from activity data.  
#
# We are going to study two of them in this workshop: Scripps clinic and Sadeh's. 

# ### Scripps Clinic Algorithm

# Scripps Clinic algorithm by Kripke was devised as a linear combination of 13 epochs (10 before the current epoch, the current epoch, 2 after the current epoch):
#
# $$
# K(T) = 0.204 \times (0.0064 \times A_{T-10} + 0.0074 \times A_{T-9} + 0.0112 \times A_{T-8} + 0.0112 \times A_{T-7} +
#         0.0118 \times A_{T-6} + 0.0118 \times A_{T-5} + 0.0128 \times A_{T-4}  + 0.0188 \times A_{T-3} + 0.0280 \times A_{T-2} + 
#         0.0664 \times A_{T-1} + 0.0300 \times A_{T} + 0.0112 \times A_{T+1} + 0.0100 \times A_{T+2})
# $$
#
#
# ###### _[Kripke,  D.  F.et al.Wrist  actigraphic  scoring  for  sleep  laboratory  patients:  algorithm  development.Journal of sleep research19, 612–619 (2010)](https://www.ncbi.nlm.nih.gov/pubmed/20408923)_    

# +
def scripps_clinic_algorithm(df, activityIdx, scaler = 0.204):
    # Enrich the dataframe with temporary values
    for i in range(1,11):
        df["_a-%d" % (i)] = df[activityIdx].shift(i).fillna(0.0)
        df["_a+%d" % (i)] = df[activityIdx].shift(-i).fillna(0.0)

    # Calculates Scripps clinic algorithm
    scripps = scaler * (0.0064 * df["_a-10"] + 0.0074 * df["_a-9"] + 0.0112 * df["_a-8"] + 0.0112 * df["_a-7"] + 0.0118 * df["_a-6"] + 0.0118 * df["_a-5"] + 0.0128 * df["_a-4"] + 0.0188 * df["_a-3"] +
                        0.0280 * df["_a-2"] + 0.0664 * df["_a-1"] + 0.0300 * df[activityIdx] + 0.0112 * df["_a+1"] + 0.0100 * df["_a+2"])

    # Deletes temporary variables
    for i in range(1,11):
        del df["_a+%d" % (i)]
        del df["_a-%d" % (i)]

    # Returns a series with binary values: 1 for sleep, 0 for awake
    return (scripps < 1.0).astype(int)

dataset1["ScrippsClinic"] = scripps_clinic_algorithm(dataset1, "ENMO_mean")

# +
dataset1.head() 

# Experiment to uncomment the next line of code 
# dataset1.tail() 

# Can you explain why ScrippsClinic algorithm classified the last events as sleep (1)?
# -

# ### Sadeh's Algorithm 

# Sadeh algorithm uses descriptive statistics (mean, var) instead of raw activity counts
#
# $$
# Sadeh(T) = 7.601 - 0.065 \times \mu(A_{Win(5)}) - 1.08  \times \mathit{NAT}({A_{Win(11)}}) - 0.056  \times \sigma(A_{Last(6)}) - 0.703 \times \ln({A_{T} + 1})
# $$
#       
#         
# ###### _[Sadeh, A., Sharkey, M. & Carskadon, M. A.  Activity-based sleep-wake identification:  an empirical test of methodological issues. Sleep 17, 201–207 (1994)](https://www.ncbi.nlm.nih.gov/pubmed/7939118)_   

# ![Features](images/features.png)

# +
def sadeh_algorithm(df, activityIdx, min_threshold=0, minNat=50, maxNat=100, window_past = 6, window_nat = 11, window_centered = 11 ):
    """
        Sadeh model for classifying sleep vs active
    """
    _mean = df[activityIdx].rolling(window=window_centered, center=True, min_periods=1).mean()
    _std = df[activityIdx].rolling(window=window_past, min_periods=1).std()
    _nat = ((df[activityIdx] >= minNat) & (df[activityIdx] <= maxNat)).rolling(window=window_nat, center=True, min_periods=1).sum()

    _LocAct = (df[activityIdx] + 1.).apply(np.log)

    sadeh = (7.601 - 0.065 * _mean - 0.056 * _std - 0.0703 * _LocAct - 1.08 * _nat)

    # Returns a series with binary values: 1 for sleep, 0 for awake
    return (sadeh > min_threshold).astype(int)

dataset1["Sadeh"] = sadeh_algorithm(dataset1, "ENMO_mean")
# -

# Unfortunately, we do not have PSG ground truth in our dataset. 
# Thus, we are going to use in this workshop the [sleep diary](https://en.wikipedia.org/wiki/Sleep_diary "Sleep Diary Wikipedia") ground truth provided by the data donor.
#
# If you want to do some experiments with actual sleep-wake classification, we recommend you to have a look at our recently published paper titled [Benchmark on a large cohort for sleep-wake classification with machine learning techniques](https://www.nature.com/articles/s41746-019-0126-9). There you can find a rich dataset with sleep-wake PSG data for almost 2000 people.
#
# According to his diary, he usually sleeps between 11 pm and 6 am and has no naps during the day.

# +
start_sleep_hour = 23
end_sleep_hour = 6

df_night = dataset1[(dataset1['hour'] >= start_sleep_hour) | (dataset1['hour'] <= end_sleep_hour)]

dataset1["GroundTruth"] = 0
dataset1.loc[df_night.index, "GroundTruth"] = 1


# +
# Let's Visualize the results
def vizualize(df, cols, timecol="ts", start_sleep_hour=23, end_sleep_hour=6, colormap="Set1"):

    cmap = plt.cm.get_cmap(colormap)
    colors = cmap(np.arange(cmap.N))
    plt.rcParams['axes.facecolor'] = 'white'
    fig, axes = plt.subplots(len(cols), 1, figsize= (16, len(cols)*2.5))

    axes[0].set_title("Activity Rate per participant")
    for i in range(len(cols)):
        axes[i].tick_params(axis='x', which='both',bottom=False,top=False, labelbottom=False)
    axes[i].tick_params(axis='x', which='both',bottom=True,top=False, labelbottom=True, rotation=45)

    for i in range(len(cols)):
        axes[i].grid(color='#b2b2b7', linestyle='--',linewidth=1, alpha=0.5)
        axes[i].plot(df[timecol], df[cols[i]], label=cols[i], linewidth=1.5, color=colors[i])
        axes[i].set_ylabel(cols[i])

    
    # Code to write have the night period with gray-ish shadow
    start = 0
    end = 0
    night = (df[timecol].dt.hour >= start_sleep_hour) | (df[timecol].dt.hour <= end_sleep_hour)
    pos = date2num(df[night][timecol])
    # Print blocks of gray color every
    for i in range(0, len(pos)-1):
        if abs(pos[i] - pos[i+1]) > 0.1:
            end = i
            for ax in range(len(cols)):
                axes[ax].axvspan(pos[start], pos[end], color='#DDDDDD')
            start = i+1
    # In case the night period is in the data and we still have a last block to print
    if len(pos) > 0:
        # Print the last block
        for ax in range(len(cols)):
            axes[ax].axvspan(pos[start], pos[-1], color='#DDDDDD')
        
    
    # Center out the legend box
    for i in range(len(cols)):
        axes[i].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    # Adjust Params
    fig.autofmt_xdate()
    plt.tight_layout()
    
vizualize(dataset1, ["ScrippsClinic", "Sadeh", "GroundTruth", "ENMO_mean"])


# -

# ## Machine Learning Experiments

# ML steps - quick overview
#
# ![PSG](images/ml.png)

# +
# There are many ways to extract features for the ML task.
# Here we present two of them. The first one (extract_raw_features) extract only the raw features from the device.
# The second extract both raw and statistic (mean, std, nat) features.

def extract_raw_features(df, activityIdx, numberOfEpochs):

    features = df[[activityIdx]].copy()
    features = features.rename(columns={"ENMO_mean":"feat_a0"})

    for i in range(1, numberOfEpochs):
         features["feat_a+%d" % (i)] = features["feat_a0"].shift(i).fillna(0.0)
        
    
    names = [key for key in df.keys() if key.startswith("feat_")]

    print("Using as features:")
    for feat in names:
        print(feat)
        
    return features

def extract_all_features(df, activityIdx, minNat=50, maxNat=100, window_raw=11, window_past = 6, window_nat = 11, window_centered = 11 ):

    # Raw activity is one of the features
    df["feat_act"] = df[activityIdx]

    # From ScrippsClinicAlgorithm, we use the past and future epochs
    for i in range(1, window_raw):
        df["feat_a-%d" % (i)] = df[activityIdx].shift(i).fillna(0.0)
        df["feat_a+%d" % (i)] = df[activityIdx].shift(-i).fillna(0.0)

    # From Sadeh algorithm, we use the non-linear features
    df["feat_mean"] = df[activityIdx].rolling(window=window_centered, center=True, min_periods=1).mean().fillna(0.0)
    df["feat_std"] = df[activityIdx].rolling(window=window_past, min_periods=1).std().fillna(0.0)
    df["feat_nat"] = ((df[activityIdx] >= minNat) & (df[activityIdx] <= maxNat)).rolling(window=window_nat, center=True, min_periods=1).sum().fillna(0.0)
    df["feat_log"] = (df[activityIdx] + 1.).apply(np.log).replace([np.inf, -np.inf], np.nan).fillna(0.0)

    features = [key for key in df.keys() if key.startswith("feat_")]

    print("Using as features:")
    for feat in features:
        print(feat)

    saveToReturn = df[features]
    
    # Clean dataframe -- i.e., remove cols started by __feat*
    for feat in features:
        del df[feat] 

    return saveToReturn

# extract_raw_features(dataset1, "ENMO_mean", 5)
extract_all_features(dataset1, "ENMO_mean")



# +
# Lets use all features for the standard ML method:
X = extract_all_features(dataset1, "ENMO_mean")
X_only_raw = extract_raw_features(dataset1, "ENMO_mean", 101)
Y = dataset1["GroundTruth"]

# Let's concatenate X and Y to see the resulting dataframe:

pd.concat((X,Y), axis=1)
# -


# There are many different ways to evaluate our algorithms. Here we are going to use a simply train/test split.
#
# ![Splitting](images/splitting-data.jpg)
#
# We break down our dataset into 2 pieces: <b> train (80%) </b> and <b> test (20%) </b> set.
#
# Note that the function we used (sklearn.model_selection.train_test_split) shuffles the dataset before splitting.
# The shuffle split is usually a good practice. 
#
# Can you explain why?

# +
X_train, X_test, X_raw_train, X_raw_test, \
y_train, y_test, df_train, df_test = model_selection.train_test_split(X, X_only_raw, Y, dataset1, test_size=0.20, random_state=0)

print("Shapes:", X_train.shape, X_raw_train.shape, y_train.shape, df_train.shape)
# -

# Lets quickly inspect the data
X_train.head()


# + {"code_folding": []}
# Function to plot the results nicely.
def plot_classification_results(y_true, y_pred, normalize=False, title=None, figsize=6, cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    print("-" * 80)
    print(metrics.classification_report(y_true, y_pred))
    print("F-score:", metrics.f1_score(y_true, y_pred, average="micro"))
    
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = metrics.confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data

    classes = utils.multiclass.unique_labels(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    fig, ax = plt.subplots(figsize = (figsize, figsize))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Set plot lenght to include all classes
    ax.set_ylim(-0.5, len(classes) - 0.5)
    ax.set_xlim(-0.5, len(classes) - 0.5)
    
    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    #ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    #fig.tight_layout()
    plt.show()
    print("=" * 80)
    return ax


# -

# ### Logistic Regression

# ![LR](images/logisticregression.png)

# +
# Logistic Regression model using only the raw time series activity data
model = linear_model.LogisticRegression(random_state=42)
model.fit(X_raw_train, y_train)
y_pred = model.predict(X_raw_test)

# save predictions
df_test["LogisticRegression"] = y_pred

plot_classification_results(y_test, y_pred)

# +
# Logistic Regression model using fewer raw time series activity data and better statistics metrics
model = linear_model.LogisticRegression(random_state=42)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

# save predictions
df_test["LogisticRegression"] = y_pred

plot_classification_results(y_test, y_pred)

# +
#Show coefficients of logistic regression model
features = X_train.columns
importances = model.coef_[0][1:25]
indices = np.argsort(importances)

plt.figure(1)
plt.title('Logistic Regression Importance')
plt.barh(range(len(indices)), importances[indices], color='b', align='center')
plt.yticks(range(len(indices)), features[indices])
plt.xlabel('Coefficient value')
# -

# ### Random Forest

# ![RF](images/RF_Process.png)

# Notebook allows us to inspect the documentation of any method in a very easy way.
# Take your time to explore the documentation on the Random Forest Classifier.
# Many paramenters are presented. In experiments outside this workshop, you should aim to find
# the best values for (the most important) parameters.
ensemble.RandomForestClassifier?

# +
# Here we only play with max_depth. 
# Experiment running this cell with 'max_depth' = 1 or 10 and note how largely the Accuracy scores chances.
model = ensemble.RandomForestClassifier(max_depth=5, random_state=42)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

df_test["RandomForest"] = y_pred
plot_classification_results(y_test, y_pred)


# +
# Here we can inspect the relative feature importance of features used by random forest classifier
features = X_train.columns
importances = model.feature_importances_
indices = np.argsort(importances)

plt.figure()
plt.title('Feature Importances')
plt.barh(range(len(indices)), importances[indices], color='b', align='center')
plt.yticks(range(len(indices)), features[indices])
plt.xlabel('Relative Importance')

# +
# Visualize the 1st RF tree
export_graphviz(model.estimators_[1], 
                out_file='images/tree.dot', 
                feature_names = X_train.columns,
                class_names=["0","1"],
                rounded = True, proportion = False, 
                precision = 2, filled = True)

os.system('dot -Tpng images/tree.dot -o images/tree.png')
display.Image(filename="images/tree.png")

# +
# Finally let's see the results:
vizualize(df_test.sort_values(by="timestamp"), ["ScrippsClinic", "Sadeh", "RandomForest", "LogisticRegression", "GroundTruth", "ENMO_mean"])

# Note how the tradicional algorithms behave on the week days and on the weekends. 
# Now see the ML results for the same period.

# +
def sleep_efficiency(df, col):
    return 1.* (df[col] == 1).sum() / (df[col] == 1).shape[0]

for col in ["GroundTruth", "Sadeh", "ScrippsClinic", "RandomForest", "LogisticRegression"]:
    print("Alg: %s" % (col))
    print("Eff: %.2f" % sleep_efficiency(df_test, col))
    print("-" * 40)
    
# The sleep efficiency implementation here just calculates the percentage of time the person is sleeping.
# Ideally we want our algorithms to show an efficiency as close as the one shown by the ground truth.
# -


# # BONUS PART! 
# ## Convolutional Neural Networks

# ![CNN](images/Activity_CNN.jpg)

# CNN allows us to effectivelly use the time series data from the actigraphy device.
# That means we are able to use the features extracted from the device directly.
# Remember X_raw_train?
X_raw_train


# The input used in Keras is a numpy array instead of a pandas dataframe. We make this transformation with:
X_train_cnn = X_raw_train.values
X_test_cnn = X_raw_test.values
Y_train_cnn = y_train.values
Y_test_cnn = y_test.values

# +
from keras import models
from keras import layers

def build_model(input_shape, filters=64, kernel_size=2):
    """
       Some of the parameters of the model were parametizided here, but others could also be parameters
    """
    model = models.Sequential()
    model.add(layers.Conv1D(filters=filters, kernel_size=kernel_size, input_shape=input_shape))
    model.add(layers.Activation('relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))

    model.compile(optimizer="rmsprop", loss="binary_crossentropy", metrics=["accuracy"])
    model.summary()
    return model


# +
# model parameters
epochs = 10
batch_size = 16

# Reshape required by keras to work
X_train_reshaped = X_train_cnn.reshape(X_train_cnn.shape + (1,))
Y_train_reshaped = Y_train_cnn.reshape(-1, 1)

model = build_model(input_shape=X_train_reshaped.shape[1:], filters=32, kernel_size=10)
model.fit(X_train_reshaped, Y_train_reshaped, epochs=epochs, batch_size=batch_size, verbose=1)


# +
# Reshape required by keras to work
X_test_reshaped = X_test_cnn.reshape(X_test_cnn.shape + (1,))

# CNN calculates the probability of a class. We round this probability to get the predictions.
y_pred = np.round(model.predict(X_test_reshaped))

df_test["CNN"] = y_pred
plot_classification_results(Y_test_cnn, y_pred)

# +
# Understanding the results with summaries per day:
for day in [18, 19, 20, 21, 22, 23, 24, 25, 26]:
    df_day = df_test[(df_test["ts"].dt.day == day)] # .reset_index()
    print("Day: %d, CNN: %.2f, RF: %.2f" % 
          (day, metrics.accuracy_score(df_day["GroundTruth"], df_day["CNN"]), metrics.accuracy_score(df_day["GroundTruth"], df_day["RandomForest"])))

metrics.accuracy_score(df_test["GroundTruth"], df_test["CNN"]), metrics.accuracy_score(df_test["GroundTruth"], y_test)

# + {"code_folding": []}
# Understanding the results visually
vizualize(df_test.sort_values(by="timestamp"), [ "ScrippsClinic", "Sadeh", "CNN", "RandomForest", "LogisticRegression", "GroundTruth", "ENMO_mean"])
# -

# ### Wrap-up Discussion
#
# <b> How can we further improve the quality of these models? </b>
#  - Better algorithms? More advanced ML models (LSTM, Multi-task learning, etc...)
#  - More data? E.g., collect similar data for a dozen of people. How about a hundred people or a thousand?
#  - Have a model for weekdays and another for weekends?
